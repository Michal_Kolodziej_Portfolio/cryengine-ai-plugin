#include "StdAfx.h"
#include "FlowNodes.h"
#include "CryFlowGraph/IFlowBaseNode.h"
#include "CryFlowGraph/IFlowGraphModuleManager.h"
#include "BehaviourTree/LeafFlowgraph.h"
#include "BehaviourTree/AIFuryComponent.h"

//AIFury nodes
//BLACKBOARD
//KEY SET
class CAIFury_BlackboardKey_Int_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Int_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("IntValue", _HELP("Key value to get")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, int> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Int(keyname);
								if (pair)
								{
									std::pair<string, int> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Int_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Float_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Float_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<float>("FloatValue", _HELP("Key value to get"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, float> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Float(keyname);
								if (pair)
								{
									std::pair<string, float> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Float_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Bool_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Bool_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<bool>("BoolValue", _HELP("Key value to get"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, bool> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Bool(keyname);
								if (pair)
								{
									std::pair<string, bool> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Bool_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_String_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_String_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<string>("StringValue", _HELP("Key value to get"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, string> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_String(keyname);
								if (pair)
								{
									std::pair<string, string> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_String_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Vec3_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Vec3_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("Vec3Value", _HELP("Key value to get"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, Vec3> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Vec3(keyname);
								if (pair)
								{
									std::pair<string, Vec3> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Vec3_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_EntityId_Get : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_EntityId_Get(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			{0}
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("EntityidValue", _HELP("Key value to get")),
			{0}
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwner = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));

				if (CAIFuryComponent *aiFury_Component = pOwner->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							if (in_keyname)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								std::pair<string, EntityId> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_EntityId(keyname);
								if (pair)
								{
									std::pair<string, EntityId> pr = *pair;
									ActivateOutput(pActInfo, OUT_VALUE, pr.second);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_EntityId_Get(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//KEY GET
class CAIFury_BlackboardKey_Int_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Int_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<int>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								int val = *in_data->GetPtr<int>();

								std::pair<string, int> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Int(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Int_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Float_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Float_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<float>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<float>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								float val = *in_data->GetPtr<float>();

								std::pair<string, float> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Float(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Float_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Bool_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Bool_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<bool>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<bool>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								bool val = *in_data->GetPtr<bool>();
								std::pair<string, bool> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Bool(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Bool_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_String_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_String_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<string>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<string>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								string val = *in_data->GetPtr<string>();
								std::pair<string, string> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_String(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_String_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_Vec3_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_Vec3_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<Vec3>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								Vec3 val = *in_data->GetPtr<Vec3>();
								std::pair<string, Vec3> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_Vec3(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_Vec3_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_BlackboardKey_EntityId_Set : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_KEYNAME,
		IN_VALUE
	};
	enum Outputs
	{
		OUT_VALUE = 0,
		OUT_NOKEY
	};
public:
	CAIFury_BlackboardKey_EntityId_Set(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Get blackboard key value")),
			InputPortConfig<string>("KeyName", _HELP("Key name to get")),
			InputPortConfig<EntityId>("Value", _HELP("Key name to get"))
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("OnSet", _HELP("Key value to set")),
			OutputPortConfig<SFlowSystemVoid>("NoKeyFound", _HELP("Triggered if no key was found for this name"))
		};

		config.sDescription = "Gets blackboard key value";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
				{
					if (aiFury_Component->GetTree())
					{
						if (aiFury_Component->GetTree()->pBlackboard)
						{
							//get key name first
							TFlowInputData *in_keyname = pActInfo->GetInputPort(IN_KEYNAME);
							TFlowInputData *in_data = pActInfo->GetInputPort(IN_VALUE);
							if (in_keyname && in_data)
							{
								const string keyname = *in_keyname->GetPtr<string>();
								EntityId val = *in_data->GetPtr<EntityId>();
								std::pair<string, EntityId> *pair = aiFury_Component->GetTree()->pBlackboard->FindKey_EntityId(keyname);
								if (pair)
								{
									pair->second = val;
									ActivateOutput(pActInfo, OUT_VALUE, val);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_NOKEY, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_BlackboardKey_EntityId_Set(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//~BLACKBOARD
class CAIFury_LeafTaskStart : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_START = 0,
		IN_OWNERID
	};
	enum Outputs
	{
		OUT_LEAF_STARTED = 0
	};
public:
	CAIFury_LeafTaskStart(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Start", _HELP("Call on start")),
			InputPortConfig<EntityId>("OwnerId", _HELP("Id of owner of this module")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnStart", _HELP("Called when entire thing is done")),
			{ 0 }
		};

		config.sDescription = "Return true or false value of this module";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			TFlowInputData *in_ownerData = pActInfo->GetInputPort(IN_OWNERID);
			EntityId *value = in_ownerData->GetPtr<EntityId>();

			if (value)
			{
				if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*value))
				{
					if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
					{
						pActInfo->pGraph->SetGraphEntity(pEntity->GetId(), 0);
						ActivateOutput(pActInfo, OUT_LEAF_STARTED, 0);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LeafTaskStart(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LeafTaskEnd : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_SUCCEEDED = 0,
		IN_FAILED
	};
public:
	CAIFury_LeafTaskEnd(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Succeeded", _HELP("Call on success")),
			InputPortConfig<SFlowSystemVoid>("Failed", _HELP("Call when failed")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			{ 0 }
		};

		config.sDescription = "Return true or false value of this module";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
			if (CAIFuryComponent *aiFury_Component = pEntity->GetComponent<CAIFuryComponent>())
			{
				if (aiFury_Component->GetTree())
				{
					if (aiFury_Component->GetTree()->pBlackboard)
					{
						if (SNode *curNode = aiFury_Component->GetTree()->pBlackboard->currentNode)
						{
							if (IsPortActive(pActInfo, IN_SUCCEEDED))
							{
								aiFury_Component->GetTree()->pBlackboard->currentNode->returnValue = true;
							}
							else if (IsPortActive(pActInfo, IN_FAILED))
							{
								aiFury_Component->GetTree()->pBlackboard->currentNode->returnValue = false;
							}
						}
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LeafTaskEnd(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};

//REGISTRATORS
//AIFury nodes
//BLACKBOARD
//GET
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:Int", CAIFury_BlackboardKey_Int_Get);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:Float", CAIFury_BlackboardKey_Float_Get);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:Bool", CAIFury_BlackboardKey_Bool_Get);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:String", CAIFury_BlackboardKey_String_Get);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:Vec3", CAIFury_BlackboardKey_Vec3_Get);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeyGet:EntityId", CAIFury_BlackboardKey_EntityId_Get);
//SET
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:Int", CAIFury_BlackboardKey_Int_Set);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:Float", CAIFury_BlackboardKey_Float_Set);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:Bool", CAIFury_BlackboardKey_Bool_Set);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:String", CAIFury_BlackboardKey_String_Set);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:Vec3", CAIFury_BlackboardKey_Vec3_Set);
REGISTER_FLOW_NODE("AIFury:Blackboard:KeySet:EntityId", CAIFury_BlackboardKey_EntityId_Set);
//~BLACKBOARD
REGISTER_FLOW_NODE("AIFury:LeafTaskStart", CAIFury_LeafTaskStart);
REGISTER_FLOW_NODE("AIFury:LeafTaskEnd", CAIFury_LeafTaskEnd);
