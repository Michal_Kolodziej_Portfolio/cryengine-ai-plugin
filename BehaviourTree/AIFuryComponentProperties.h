#pragma once
#ifndef AI_FURY_PROPERTIES
#define AI_FURY_PROPERTIES
#include "FuryResources.h"

struct SAIFuryProperties
{
	inline bool operator==(const SAIFuryProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAIFuryProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	SAIFuryFile ai_behaviourTree;
};
static void ReflectType(Schematyc::CTypeDesc<SAIFuryProperties>& desc)
{
	desc.SetGUID("{4AEF9855-BE97-40FB-AE2A-1BCF0EF15391}"_cry_guid);
	desc.AddMember(&SAIFuryProperties::ai_behaviourTree, 'aifp', "AIFuryBTPath", "Behaviour tree path", "AI behaviour tree path", "");
}
#endif
