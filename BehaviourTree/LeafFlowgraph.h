#pragma once
#include "Leaf.h"
class CLeafFlowgraph : public CLeaf
{
public:
	CLeafFlowgraph::CLeafFlowgraph(CBlackboard *pBB = nullptr) { pBlackboard = pBB; }
	CLeafFlowgraph::CLeafFlowgraph(SNode *pParentTask, CBlackboard *pBB, string moduleName);
	virtual bool CheckConditions() override;
	virtual void DoAction() override;
	virtual void ReRun() override;
private:
	IFlowGraphModuleManager *pFgModuleManager = nullptr;
	IFlowGraphModule *pModule = nullptr;
};