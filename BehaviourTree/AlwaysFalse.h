#pragma once
#include "Node.h"

class CAlwaysFalse : public SNode
{
public:
	CAlwaysFalse::CAlwaysFalse(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CAlwaysFalse::CAlwaysFalse(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CAlwaysFalse::CAlwaysFalse(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
	virtual void ChildInProcess() override;
};