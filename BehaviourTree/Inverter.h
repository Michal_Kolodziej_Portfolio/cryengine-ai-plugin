#pragma once
#include "Node.h"

class CInverter : public SNode
{
public:
	CInverter::CInverter(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CInverter::CInverter(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CInverter::CInverter(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
	virtual void ChildInProcess() override;
};