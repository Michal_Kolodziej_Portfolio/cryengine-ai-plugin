#pragma once
#include <array>
#include <numeric>
#include <CryEntitySystem/IEntityComponent.h>
#include <CryMath/Cry_Camera.h>
#include <ICryMannequin.h>
#include "BehaviourTree/Sequence.h"
#include "BehaviourTree/Selector.h"
#include "BehaviourTree/TreeAsLeaf.h"
#include "BehaviourTree/Inverter.h"
#include "AIFuryComponentProperties.h"

class CBlackboard;
struct LNRegistrator;

class CAIFuryComponent : public IEntityComponent
{
public:
	//ComponentSystem
	CAIFuryComponent() = default;
	virtual ~CAIFuryComponent() {}
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CAIFuryComponent>& desc);
	//CAICharacterComponent *GetAICharacterComponent()
	//{
	//	return GetEntity()->GetComponent<CAICharacterComponent>();
	//}
	//
	//BEHAVIOUR TREE HANDLING
	void RunBehaviourTree();
	SAIFuryProperties GetProperties() { return properties; }
private:
	SAIFuryProperties properties;
public:
	//BEHAVIOUR TREE CLASS
	class CBehaviourTree
	{
	public:
		CBehaviourTree::CBehaviourTree() {}
		void Create(CAIFuryComponent *pai);
		SNode *CreateLeafNode(SNode *pNewParent, CBlackboard *pBB, string sNodeName);
		CBlackboard *pBlackboard = nullptr;
		SNode *pMainNode = nullptr;
		bool bIsReady = false;
	};
	CBehaviourTree *GetTree() { return pTree; }
	CBehaviourTree *pTree = nullptr;
	static LNRegistrator lnreg;
};