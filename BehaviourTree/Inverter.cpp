#include "Inverter.h"
#include "Sequence.h"
#include "Selector.h"

CInverter::CInverter(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	if (XmlNodeRef pNewChild = pNode->getChild(0))
	{
		if (pNewChild->getTag() == pBB->selectorStr)
			AddChild(new CSelector(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->sequenceStr)
			AddChild(new CSequence(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->leafStr)
			AddChild(new CLeafFlowgraph(this, pBlackboard, pNewChild->getAttr("name")));

		if (pNewChild->getTag() == pBlackboard->treeLeafStr)
			AddChild(new CTreeNode(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->inverterStr)
			AddChild(new CInverter(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysTrueStr)
			AddChild(new CAlwaysTrue(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysFalseStr)
			AddChild(new CAlwaysFalse(this, pBlackboard, pNewChild));
	}
}

void CInverter::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CInverter::ChildFailed()
{
	if (pParent)
	{
		//CryLogAlways("Child %d failed", iCurChildIndex);
		pParent->ChildSucceeded();
	}
	else
	{
		//CryLogAlways("The last sequence (first) failed");
		//At this point the whole behaviour tree should reset and start over - LET AI KNOW
	}
}

void CInverter::ChildSucceeded()
{
	if (pParent)
	{
		//CryLogAlways("Last child succeeded");
		pParent->ChildFailed();
	}
	else
	{
		//CryLogAlways("Last child in last sequence (first) succeded");
		//At this point the whole behaviour tree should reset and start over - LET AI KNOW
	}
}

void CInverter::ChildInProcess()
{
}
