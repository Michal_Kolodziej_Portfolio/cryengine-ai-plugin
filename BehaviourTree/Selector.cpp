#include "StdAfx.h"
#include "Selector.h"
#include "Sequence.h"
#include "Inverter.h"

CSelector::CSelector(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	for (int i = 0; i < pNode->getChildCount(); i++)
	{
		if (XmlNodeRef pNewChild = pNode->getChild(i))
		{
			if (pNewChild->getTag() == pBB->selectorStr)
				AddChild(new CSelector(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBB->sequenceStr)
				AddChild(new CSequence(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBB->leafStr)
				AddChild(new CLeafFlowgraph(this, pBlackboard, pNewChild->getAttr("name")));

			if (pNewChild->getTag() == pBlackboard->treeLeafStr)
				AddChild(new CTreeNode(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->inverterStr)
				AddChild(new CInverter(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->alwaysTrueStr)
				AddChild(new CAlwaysTrue(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->alwaysFalseStr)
				AddChild(new CAlwaysFalse(this, pBlackboard, pNewChild));
		}
	}
}

void CSelector::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CSelector::ChildFailed()
{
	if (iCurChildIndex == GetLastChildIndex())
	{
		if (pParent)
		{
			//CryLogAlways("Last child in selector failed");
			pParent->ChildFailed();
		}
		else
		{
			//CryLogAlways("The last child of the last selector failed");
			//At this point the whole behaviour tree should reset and start over - LET AI KNOW
		}
	}
	else
	{
		iCurChildIndex += 1;
		pChild[iCurChildIndex]->Start();
	}
}

void CSelector::ChildSucceeded()
{
	if (pParent)
	{
		//CryLogAlways("Child %d succeeded", iCurChildIndex);
		pParent->ChildSucceeded();
	}
	else
	{
		//CryLogAlways("Child %d succeeded in selector (first)", iCurChildIndex);
		//At this point the whole behaviour tree should reset and start over - LET AI KNOW
	}
}
