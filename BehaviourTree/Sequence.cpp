#include "StdAfx.h"
#include "Sequence.h"
#include "Selector.h"
#include "Inverter.h"

CSequence::CSequence(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	for (int i = 0; i < pNode->getChildCount(); i++)
	{
		if (XmlNodeRef pNewChild = pNode->getChild(i))
		{
			if (pNewChild->getTag() == pBB->selectorStr)
				AddChild(new CSelector(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBB->sequenceStr)
				AddChild(new CSequence(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBB->leafStr)
				AddChild(new CLeafFlowgraph(this, pBlackboard, pNewChild->getAttr("name")));

			if (pNewChild->getTag() == pBlackboard->treeLeafStr)
				AddChild(new CTreeNode(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->inverterStr)
				AddChild(new CInverter(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->alwaysTrueStr)
				AddChild(new CAlwaysTrue(this, pBlackboard, pNewChild));

			if (pNewChild->getTag() == pBlackboard->alwaysFalseStr)
				AddChild(new CAlwaysFalse(this, pBlackboard, pNewChild));
		}
	}
}

void CSequence::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CSequence::ChildFailed()
{
	if (pParent)
	{
		//CryLogAlways("Child %d failed", iCurChildIndex);
		pParent->ChildFailed();
	}
	else
	{
		//CryLogAlways("The last sequence (first) failed");
		//At this point the whole behaviour tree should reset and start over - LET AI KNOW
	}
}

void CSequence::ChildSucceeded()
{
	iChildCount;
	if (iCurChildIndex == GetLastChildIndex())
	{
		if (pParent)
		{
			//CryLogAlways("Last child succeeded");
			pParent->ChildSucceeded();
		}
		else
		{
			//CryLogAlways("Last child in last sequence (first) succeded");
			//At this point the whole behaviour tree should reset and start over - LET AI KNOW
		}
	}
	else
	{
		iCurChildIndex += 1;
		pChild[iCurChildIndex]->Start();
	}
}

void CSequence::ChildInProcess()
{

}
