#include "AlwaysTrue.h"
#include "AlwaysFalse.h"
#include "Inverter.h"
#include "Sequence.h"
#include "Selector.h"
#include "LeafFlowgraph.h"
#include "TreeAsLeaf.h"

CAlwaysTrue::CAlwaysTrue(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	if (XmlNodeRef pNewChild = pNode->getChild(0))
	{
		if (pNewChild->getTag() == pBB->selectorStr)
			AddChild(new CSelector(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->sequenceStr)
			AddChild(new CSequence(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->leafStr)
			AddChild(new CLeafFlowgraph(this, pBlackboard, pNewChild->getAttr("name")));

		if (pNewChild->getTag() == pBlackboard->treeLeafStr)
			AddChild(new CTreeNode(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->inverterStr)
			AddChild(new CInverter(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysTrueStr)
			AddChild(new CAlwaysTrue(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysFalseStr)
			AddChild(new CAlwaysFalse(this, pBlackboard, pNewChild));
	}
}

void CAlwaysTrue::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CAlwaysTrue::ChildFailed()
{
	if (pParent)
	{
		pParent->ChildSucceeded();
	}
}

void CAlwaysTrue::ChildSucceeded()
{
	if (pParent)
	{
		pParent->ChildSucceeded();
	}
}

void CAlwaysTrue::ChildInProcess()
{
}
