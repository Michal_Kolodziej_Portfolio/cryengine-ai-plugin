#include "StdAfx.h"
#include "TreeAsLeaf.h"
#include "Sequence.h"
#include "Selector.h"

CTreeNode::CTreeNode(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	XmlNodeRef pFuryTree = gEnv->pSystem->LoadXmlFromFile(pNode->getAttr("path"));
	if (pFuryTree)
	{
		XmlNodeRef pRoot = pFuryTree->getChild(0);
		if (pRoot)
		{
			XmlNodeRef pMain = pRoot->getChild(0);
			if (pMain)
			{
				if (pMain->getTag() == pBlackboard->selectorStr)
					AddChild(new CSelector(this, pBlackboard, pMain));

				if (pMain->getTag() == pBlackboard->sequenceStr)
					AddChild(new CSequence(this, pBlackboard, pMain));

				if (pMain->getTag() == pBlackboard->leafStr)
					AddChild(new CLeafFlowgraph(this, pBlackboard, pMain->getAttr("name")));

				if (pMain->getTag() == pBlackboard->treeLeafStr)
					AddChild(new CTreeNode(this, pBlackboard, pMain));

				if (pMain->getTag() == pBlackboard->inverterStr)
					AddChild(new CInverter(this, pBlackboard, pMain));

				if (pMain->getTag() == pBlackboard->alwaysTrueStr)
					AddChild(new CAlwaysTrue(this, pBlackboard, pMain));

				if (pMain->getTag() == pBlackboard->alwaysFalseStr)
					AddChild(new CAlwaysFalse(this, pBlackboard, pMain));
			}
		}
		//load blackboard from xml
		if (pFuryTree->getChildCount() > 1)
		{
			XmlNodeRef pBlackboardXmlNode = pFuryTree->getChild(1);
			if (pBlackboardXmlNode)
			{
				for (int i = 0; i < pBlackboardXmlNode->getChildCount(); i++)
				{
					if (XmlNodeRef bbKey = pBlackboardXmlNode->getChild(i))
					{
						pBlackboard->AddKey(bbKey->getTag(), bbKey->getAttr("name"));
					}
				}
			}
		}
	}
}

void CTreeNode::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CTreeNode::ChildFailed()
{
	if (pParent)
	{
		//CryLogAlways("Child %d failed", iCurChildIndex);
		pParent->ChildFailed();
	}
	else
	{
		//CryLogAlways("The last sequence (first) failed");
		//At this point the whole behaviour tree should reset and start over - LET AI KNOW
	}
}

void CTreeNode::ChildSucceeded()
{
	iChildCount;
	if (iCurChildIndex == GetLastChildIndex())
	{
		if (pParent)
		{
			//CryLogAlways("Last child succeeded");
			pParent->ChildSucceeded();
		}
		else
		{
			//CryLogAlways("Last child in last sequence (first) succeded");
			//At this point the whole behaviour tree should reset and start over - LET AI KNOW
		}
	}
	else
	{
		iCurChildIndex += 1;
		pChild[iCurChildIndex]->Start();
	}
}

void CTreeNode::ChildInProcess()
{

}
