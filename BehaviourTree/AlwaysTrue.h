#pragma once
#include "Node.h"

class CAlwaysTrue : public SNode
{
public:
	CAlwaysTrue::CAlwaysTrue(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CAlwaysTrue::CAlwaysTrue(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CAlwaysTrue::CAlwaysTrue(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
	virtual void ChildInProcess() override;
};