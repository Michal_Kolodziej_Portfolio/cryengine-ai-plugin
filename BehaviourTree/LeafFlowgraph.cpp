#include "LeafFlowgraph.h"
#include "CryFlowGraph/IFlowGraphModuleManager.h"
#include "CryFlowGraph/IFlowBaseNode.h"
#include "CryFlowGraph/IFlowSystem.h"

CLeafFlowgraph::CLeafFlowgraph(SNode * pParentTask, CBlackboard * pBB, string moduleName)
{
	pParent = pParentTask; 
	pBlackboard = pBB;
	pFgModuleManager = gEnv->pFlowSystem->GetIModuleManager();
	pModule = pFgModuleManager->GetModule(moduleName);
	name = moduleName;
}

bool CLeafFlowgraph::CheckConditions()
{
	return true;
}

void CLeafFlowgraph::DoAction()
{
	pBlackboard->currentNode = this;

	if (pModule && pBlackboard->GetAIParent())
	{
		pModule->CallDefaultInstanceForEntity(pBlackboard->GetAIParent()->GetEntity());
		return;
	}
	else
	{
		LeafFailed();
	}
}

void CLeafFlowgraph::ReRun()
{
	pBlackboard->currentNode = nullptr;

	if (returnValue)
		LeafSucceeded();
	else
		LeafFailed();
}
