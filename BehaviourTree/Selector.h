#pragma once
#include "Node.h"

class CSelector : public SNode
{
public:
	CSelector::CSelector(CBlackboard *pBB)
	{
		for (int i = 0; i < 10; i++)
		{
			pChild[i] = nullptr;
		}
		pBlackboard = pBB;
	}
	CSelector::CSelector(SNode *pParentTask, CBlackboard *pBB)
	{
		for (int i = 0; i < 10; i++)
		{
			pChild[i] = nullptr;
		}
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CSelector::CSelector(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
};