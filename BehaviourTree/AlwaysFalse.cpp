#include "AlwaysTrue.h"
#include "AlwaysFalse.h"
#include "Inverter.h"
#include "Sequence.h"
#include "Selector.h"
#include "LeafFlowgraph.h"
#include "TreeAsLeaf.h"

CAlwaysFalse::CAlwaysFalse(SNode * pParentTask, CBlackboard * pBB, XmlNodeRef pNode)
{
	pParent = pParentTask;
	pBlackboard = pBB;
	if (XmlNodeRef pNewChild = pNode->getChild(0))
	{
		if (pNewChild->getTag() == pBB->selectorStr)
			AddChild(new CSelector(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->sequenceStr)
			AddChild(new CSequence(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBB->leafStr)
			AddChild(new CLeafFlowgraph(this, pBlackboard, pNewChild->getAttr("name")));

		if (pNewChild->getTag() == pBlackboard->treeLeafStr)
			AddChild(new CTreeNode(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->inverterStr)
			AddChild(new CInverter(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysTrueStr)
			AddChild(new CAlwaysTrue(this, pBlackboard, pNewChild));

		if (pNewChild->getTag() == pBlackboard->alwaysFalseStr)
			AddChild(new CAlwaysFalse(this, pBlackboard, pNewChild));
	}
}

void CAlwaysFalse::Start()
{
	if (pChild[0])
	{
		iCurChildIndex = 0;
		pChild[0]->Start();
	}
}

void CAlwaysFalse::ChildFailed()
{
	if (pParent)
	{
		pParent->ChildFailed();
	}
}

void CAlwaysFalse::ChildSucceeded()
{
	if (pParent)
	{
		pParent->ChildFailed();
	}
}

void CAlwaysFalse::ChildInProcess()
{
}
