#include "StdAfx.h"
#include "Plugin.h"
#include "AIFuryComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include <CrySchematyc/Env/Elements/EnvFunction.h>
#include <CryRenderer/IRenderAuxGeom.h>
#include "CryFlowGraph/IFlowGraphModuleManager.h"
#include "LeafFlowgraph.h"
#include "AlwaysTrue.h"
#include "AlwaysFalse.h"
#include "FuryResources.h"

static void RegisterAIComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAIFuryComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAIComponent);

void CAIFuryComponent::Initialize()
{
}

uint64 CAIFuryComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CAIFuryComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		RunBehaviourTree();
	}
	break;
	};
}

void CAIFuryComponent::ReflectType(Schematyc::CTypeDesc<CAIFuryComponent>& desc)
{
	desc.SetGUID("{19D51A0C-AD7F-4B9A-A0F0-2E34EB8FB16A}"_cry_guid);
	desc.SetEditorCategory("AI");
	desc.SetLabel("AI Fury Component");
	desc.SetDescription("AI Fury behaviour trees component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CAIFuryComponent::properties, 'apro', "AIProperties", "AIFury settings", "Properties and settings of AIFury component", SAIFuryProperties());
}

void CAIFuryComponent::RunBehaviourTree()
{
	if (CAIFury::ALLOW_PLUGIN)
	{
		if (pTree)
		{
			if (pTree->bIsReady)
			{
				if (!pTree->pBlackboard->currentNode)
					pTree->pMainNode->Start();
				else
					pTree->pBlackboard->currentNode->ReRun();
			}
		}
		else
		{
			pTree = new CBehaviourTree();
			pTree->Create(this);
		}
	}
}

void CAIFuryComponent::CBehaviourTree::Create(CAIFuryComponent * pai)
{
	//CREATE BLACKBOARD
	pBlackboard = new CBlackboard(pai);
	//CREATE TREE
	//FROM XML
	XmlNodeRef pFuryTree = gEnv->pSystem->LoadXmlFromFile(pai->GetProperties().ai_behaviourTree.value.c_str());
	if (pFuryTree)
	{
		XmlNodeRef pRoot = pFuryTree->getChild(0);
		if (pRoot)
		{
			XmlNodeRef pMain = pRoot->getChild(0);
			if (pMain)
			{
				if (pMain->getTag() == pBlackboard->selectorStr)
					pMainNode = new CSelector(nullptr, pBlackboard, pMain);

				if (pMain->getTag() == pBlackboard->sequenceStr)
					pMainNode = new CSequence(nullptr, pBlackboard, pMain);

				if (pMain->getTag() == pBlackboard->leafStr)
					pMainNode = new CLeafFlowgraph(nullptr, pBlackboard, pMain->getAttr("name"));

				if (pMain->getTag() == pBlackboard->treeLeafStr)
					pMainNode = new CTreeNode(nullptr, pBlackboard, pMain);

				if (pMain->getTag() == pBlackboard->inverterStr)
					pMainNode = new CInverter(nullptr, pBlackboard, pMain);

				if (pMain->getTag() == pBlackboard->alwaysTrueStr)
					pMainNode = new CAlwaysTrue(nullptr, pBlackboard, pMain);

				if (pMain->getTag() == pBlackboard->alwaysFalseStr)
					pMainNode = new CAlwaysFalse(nullptr, pBlackboard, pMain);

				bIsReady = true;
			}
		}
		//load blackboard from xml
		if (pFuryTree->getChildCount() > 1)
		{
			XmlNodeRef pBlackboardXmlNode = pFuryTree->getChild(1);
			if (pBlackboardXmlNode)
			{
				for (int i = 0; i < pBlackboardXmlNode->getChildCount(); i++)
				{
					if (XmlNodeRef bbKey = pBlackboardXmlNode->getChild(i))
					{
						pBlackboard->AddKey(bbKey->getTag(), bbKey->getAttr("name"));
					}
				}
			}
		}
	}
}