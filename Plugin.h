#pragma once

#include <CrySystem/ICryPlugin.h>

#include <CryGame/IGameFramework.h>

#include <CryEntitySystem/IEntityClass.h>
#include "FlashUI/FlashUI.h"

class CAITagPointComponent;
class CAIFuryComponent;
class CAIFury_LeafTaskEnd;

class CAIFury 
	: public ICryPlugin
	, public ISystemEventListener
	, public IUIElementEventListener
{
public:
	CRYINTERFACE_SIMPLE(ICryPlugin)
	CRYGENERATE_SINGLETONCLASS_GUID(CAIFury, "AIFury", "9BC6F648-184F-4000-88F1-1CBDBC5EB133"_cry_guid)

	virtual void OnUIEventEx(IUIElement* pSender, const char* fscommand, const SUIArguments& args, void* pUserData);

	virtual ~CAIFury();
	CAIFury();
	bool RegisterFlowNodes();
	bool UnregisterFlowNodes();
	
	//! Retrieve name of plugin.
	virtual const char* GetName() const override { return "AIFury"; }

	//! Retrieve category for the plugin.
	virtual const char* GetCategory() const override { return "Game"; }

	//! This is called to initialize the new plugin.
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;

	virtual void OnPluginUpdate(EPluginUpdateType updateType) override;

	// ISystemEventListener
	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener
	static void AddTagPoint(CAITagPointComponent *pNewTagPoint)
	{
		if (!pNewTagPoint)
			return;

		for (int i = 0; i < pTagPointsCount + 1; i++)
		{
			if (pTagPointsList[i] == nullptr)
			{
				pTagPointsCount += 1;
				pTagPointsList[i] = pNewTagPoint;
				break;
			}
		}
	}

	static CAITagPointComponent *pTagPointsList[10000];
	static int pTagPointsCount;
	static bool ALLOW_PLUGIN;

	IUIElement *Element = nullptr;

	void InitializeUI();
	void CreateUIFiles();
};