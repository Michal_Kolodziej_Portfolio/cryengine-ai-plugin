#pragma once
#include <CrySerialization/IArchive.h>
#include <CrySerialization/Decorators/ResourceFilePath.h>
#include <CrySerialization/Decorators/Resources.h>
#include <CrySerialization/Decorators/ResourcesAudio.h>
#include <CrySerialization/Decorators/ActionButton.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CrySerialization/Decorators/ResourceSelector.h>
#include <CrySerialization/Decorators/ResourceFilePath.h>

template<Serialization::ResourceSelector<string>(*SELECTOR)(string&)> struct SAnyFileSelector
{
	inline SAnyFileSelector() {}
	inline SAnyFileSelector(const char* _sValue)
		: value(_sValue)
	{}
	inline bool operator==(const SAnyFileSelector& rhs) const
	{
		return value == rhs.value;
	}

	string value;
};
template<Serialization::ResourceSelector<string>(*SELECTOR)(string&)> inline bool Serialize(Serialization::IArchive& archive, SAnyFileSelector<SELECTOR>& value, const char* sName, const char* sLabel)
{
	archive(SELECTOR(value.value), sName, sLabel);
	return true;
}
typedef SAnyFileSelector<&Serialization::GeneralFilename<string>> SAnyFile;
inline void ReflectType(Schematyc::CTypeDesc<SAnyFile>& desc)
{
	desc.SetGUID("{CD318BC9-AC5C-4560-BA20-4BF4A30A3E2F}"_cry_guid);
	desc.SetLabel("AnyFile");
	desc.SetDescription("Any file name");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T> Serialization::ResourceFilePath FuryFilename(string& value) { return Serialization::ResourceFilePath(value, "FireMode (fmd)|*.fmd"); }

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> struct SFuryFilePath
{
	inline SFuryFilePath() {}

	inline SFuryFilePath(const char* _szValue)
		: value(_szValue)
	{}

	inline bool operator==(const SFuryFilePath& rhs) const
	{
		return value == rhs.value;
	}

	string value;
};

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> inline bool Serialize(Serialization::IArchive& archive, SFuryFilePath<SERIALIZER>& value, const char* szName, const char* szLabel)
{
	archive(SERIALIZER(value.value), szName, szLabel);
	return true;
}
typedef SFuryFilePath<&FuryFilename<string>> SFuryFile;
inline void ReflectType(Schematyc::CTypeDesc<SFuryFile>& desc)
{
	desc.SetGUID("{B60BFDC4-364D-4B70-A144-6558D76F3DEB}"_cry_guid);
	desc.SetLabel("FuryFile");
	desc.SetDescription("Fury file name");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T> Serialization::ResourceFilePath AIFuryFileName(string& value) { return Serialization::ResourceFilePath(value, "AIFuryFile (fury)|*.fury"); }

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> struct SAIFuryFilePath
{
	inline SAIFuryFilePath() {}

	inline SAIFuryFilePath(const char* _szValue)
		: value(_szValue)
	{}

	inline bool operator==(const SAIFuryFilePath& rhs) const
	{
		return value == rhs.value;
	}

	string value;
};

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> inline bool Serialize(Serialization::IArchive& archive, SAIFuryFilePath<SERIALIZER>& value, const char* szName, const char* szLabel)
{
	archive(SERIALIZER(value.value), szName, szLabel);
	return true;
}
typedef SAIFuryFilePath<&AIFuryFileName<string>> SAIFuryFile;
inline void ReflectType(Schematyc::CTypeDesc<SAIFuryFile>& desc)
{
	desc.SetGUID("{B60BFDC4-364D-4B70-A144-6558D76F3DEB}"_cry_guid);
	desc.SetLabel("AIFuryFile");
	desc.SetDescription("AIFury file");
}
//Sounds
template<Serialization::ResourceSelector<string>(*SERIALIZER) (string&)> struct SSoundFileSelector
{
	inline SSoundFileSelector() {}

	inline SSoundFileSelector(const char* _szValue)
		: value(_szValue)
	{}

	inline bool operator==(const SSoundFileSelector& rhs) const
	{
		return value == rhs.value;
	}

	string value;
};

template<Serialization::ResourceSelector<string>(*SERIALIZER) (string&)> inline bool Serialize(Serialization::IArchive& archive, SSoundFileSelector<SERIALIZER>& value, const char* szName, const char* szLabel)
{
	archive(SERIALIZER(value.value), szName, szLabel);
	return true;
}
typedef SSoundFileSelector<&Serialization::SoundFilename<string>> SSoundFile;
inline void ReflectType(Schematyc::CTypeDesc<SSoundFile>& desc)
{
	desc.SetGUID("{91E167AE-18E3-4D26-9B61-CF74C527F483}"_cry_guid);
	desc.SetLabel("SoundFile");
	desc.SetDescription("Sound file name");
}
//Sound path to xml file of mixer
template<class T> Serialization::ResourceFilePath XmlFilePicker(string& value) { return Serialization::ResourceFilePath(value, "XmlFile (xml)|*.xml"); }

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> struct FuryXmlFile
{
	inline FuryXmlFile() {}

	inline FuryXmlFile(const char* _szValue)
		: value(_szValue)
	{}

	inline bool operator==(const FuryXmlFile& rhs) const
	{
		return value == rhs.value;
	}

	string value;
};

template<Serialization::ResourceFilePath(*SERIALIZER) (string&)> inline bool Serialize(Serialization::IArchive& archive, FuryXmlFile<SERIALIZER>& value, const char* szName, const char* szLabel)
{
	archive(SERIALIZER(value.value), szName, szLabel);
	return true;
}
typedef FuryXmlFile<&XmlFilePicker<string>> XmlFile;
inline void ReflectType(Schematyc::CTypeDesc<XmlFile>& desc)
{
	desc.SetGUID("{8BC627DE-4E73-4098-A715-89924BBB241C}"_cry_guid);
	desc.SetLabel("XmlFile");
	desc.SetDescription("Xml file name");
}